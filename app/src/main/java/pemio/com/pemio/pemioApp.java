package pemio.com.pemio;

import android.app.Application;

import com.facebook.stetho.Stetho;

public class pemioApp extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        if(Constants.isDebug){
            Stetho.initializeWithDefaults(this);
        }
    }
}
