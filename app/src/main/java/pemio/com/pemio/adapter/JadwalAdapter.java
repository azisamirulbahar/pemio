package pemio.com.pemio.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import pemio.com.pemio.model.Jadwal;
import pemio.com.pemio.R;
import pemio.com.pemio.model.Jadwal;

public class JadwalAdapter extends RecyclerView.Adapter<JadwalAdapter.JadwalHolder> {
    public interface onClickItem {
        void onClick(int pos);
    }

    List<Jadwal> jadwalList;
    Context context;
    onClickItem onClickItemListener;


    public JadwalAdapter(Context context, List<Jadwal> jadwalList, onClickItem listener) {
        this.jadwalList = jadwalList;
        this.context = context;
        onClickItemListener = listener;
    }

    @Override
    public JadwalHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_jadwal, parent, false);

        return new JadwalHolder(v);
    }

    @Override
    public void onBindViewHolder(JadwalHolder holder, int position) {

        holder.bindView(jadwalList.get(position));
    }

    @Override
    public int getItemCount() {
        if (jadwalList == null) {
            return 0;
        } else {
            return jadwalList.size();
        }


    }

    public void addItem(Jadwal j) {
        jadwalList.add(j);
        notifyDataSetChanged();
    }

    public void addItem(List<Jadwal> j) {
        jadwalList.addAll(j);
        notifyDataSetChanged();
    }

    public void removeItem(int pos) {
        jadwalList.remove(pos);
        notifyItemRemoved(pos);
    }

    public class JadwalHolder extends RecyclerView.ViewHolder {
        private TextView tvNamaPenyakit;
        private TextView tvNamaObat;
        private TextView tvJam;


        public JadwalHolder(View itemView) {
            super(itemView);
            tvNamaPenyakit = itemView.findViewById(R.id.list_nama_penyakit);
            tvNamaObat = itemView.findViewById(R.id.list_nama_obat);
            tvJam = itemView.findViewById(R.id.list_jam);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onClickItemListener.onClick(getAdapterPosition());
                }
            });
        }

        public void bindView(Jadwal jadwal) {
            tvNamaPenyakit.setText(jadwal.getPenyakit());
            tvNamaObat.setText(jadwal.getNamaObat());
            tvJam.setText(jadwal.getJamMinum());

        }
    }
}
