package pemio.com.pemio.converter;

public  class Converter {
    public static String convertDigit(int digit){
        if (String.valueOf(digit).length() < 2) {
            return "0" + digit;
        }else{
            return String.valueOf(digit);
        }
    }
}
