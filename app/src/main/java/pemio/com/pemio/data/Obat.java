
package pemio.com.pemio.data;
import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.content.ContentValues;
import android.provider.BaseColumns;

@Entity(tableName = Obat.TABLE_NAME)
public class Obat {
    public static final String TABLE_NAME="pemioDB";

    public static final String COLUMN_ID= BaseColumns._ID;

    public static final String COLUMN_OBAT="obat";
    public static final String COLUMN_PENYAKIT="penyakit";
    public static final String COLUMN_JAM="jam";
    public static final String COLUMN_MENIT="menit";
    public static final String COLUMN_SIGNA="signa";
    public static final String COLUMN_INSERT_TIME="insert_time";



    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(index = true,name=COLUMN_ID)
    public long id;

    @ColumnInfo(name=COLUMN_OBAT)
    public String obat;

    @ColumnInfo(name=COLUMN_PENYAKIT)
    public String penyakit;

    @ColumnInfo(name=COLUMN_JAM)
    public int jam;

    @ColumnInfo(name=COLUMN_MENIT)
    public int menit;

    @ColumnInfo(name=COLUMN_SIGNA)
public int signa;

    @ColumnInfo(name=COLUMN_INSERT_TIME)
    public long insertTime;

    public static Obat fromContentValues(ContentValues contentValues){
        final Obat obat=new Obat();
        if(contentValues.containsKey(COLUMN_ID)){
            obat.id=contentValues.getAsLong(COLUMN_ID);
        }
        if(contentValues.containsKey(COLUMN_OBAT)){
            obat.obat=contentValues.getAsString(COLUMN_OBAT);
        }
        if(contentValues.containsKey(COLUMN_PENYAKIT)){
            obat.penyakit=contentValues.getAsString(COLUMN_PENYAKIT);
        }
        if(contentValues.containsKey(COLUMN_JAM)){
            obat.jam=contentValues.getAsInteger(COLUMN_JAM);
        }
        if(contentValues.containsKey(COLUMN_MENIT)){
            obat.menit=contentValues.getAsInteger(COLUMN_MENIT);
        }
        if(contentValues.containsKey(COLUMN_SIGNA)){
            obat.signa=contentValues.getAsInteger(COLUMN_SIGNA);
        }
        if(contentValues.containsKey(COLUMN_INSERT_TIME)){
            obat.insertTime=contentValues.getAsLong(COLUMN_INSERT_TIME);
        }
        return obat;
    }
}
