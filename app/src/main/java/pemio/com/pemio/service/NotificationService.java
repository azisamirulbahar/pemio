package pemio.com.pemio.service;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.JobIntentService;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import pemio.com.pemio.Constants;
import pemio.com.pemio.DataParser;
import pemio.com.pemio.R;
import pemio.com.pemio.activity.DetailActivity;
import pemio.com.pemio.activity.MainActivity;
import pemio.com.pemio.data.Obat;
import pemio.com.pemio.provider.PemioProvider;
import pemio.com.pemio.receiver.PemioReceiver;
import pemio.com.pemio.reminder.ReminderSetting;

public class NotificationService extends JobIntentService {
    PendingIntent contentPendingIntent;
    NotificationManager notificationManager;
    int code;
    static int JOB_ID = 1000;


    public static void enqueueWork(Context context, Intent work) {
        enqueueWork(context, NotificationService.class, JOB_ID, work);
    }

    @Override
    protected void onHandleWork(@NonNull Intent intent) {
        if (intent.hasExtra(Constants.EXTRA_IS_AFTER_BOOT)) {
            if (intent.getBooleanExtra(Constants.EXTRA_IS_AFTER_BOOT, false)) {
                String[] projection = new String[]{Obat.COLUMN_ID, Obat.COLUMN_OBAT, Obat.COLUMN_PENYAKIT,Obat.COLUMN_JAM,Obat.COLUMN_MENIT};

                Cursor c = getContentResolver().query(PemioProvider.URI_PEMIO, projection, null, null, null);

                if (c != null) {
                    if (c.moveToFirst()) {
                        while (!c.isAfterLast()) {
                            Obat o = new Obat();
                            DataParser.parse(c, o);
                            int idNotif = (int) o.id;
                            ReminderSetting.setReminder(this, PemioReceiver.class, o.jam, o.menit, idNotif);


                            c.moveToNext();
                        }


                        c.close();
                    }
                } else {
                    Log.d("cursor is null", "cursor");
                }
            }

        } else {

            notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

            Intent contentIntnt = new Intent(this, MainActivity.class);
            code = intent.getIntExtra(Constants.EXTRA_CODE_REQUEST, 1);
            contentPendingIntent = PendingIntent.getActivity(this, code, contentIntnt, PendingIntent.FLAG_ONE_SHOT);

            Log.d("requestcode", String.valueOf(code));

            String[] projection = new String[]{Obat.COLUMN_ID, Obat.COLUMN_OBAT, Obat.COLUMN_PENYAKIT};
            Uri uri = Uri.parse(PemioProvider.URI_PEMIO + "/" + code);
            Cursor c = getContentResolver().query(uri, projection, null, null, null);
            Obat o = new Obat();
            if (c != null) {
                if (c.moveToFirst()) {
                    DataParser.parse(c, o);
                    createNotification((int) o.id, "Jangan lupa minum obat : " + o.obat);
                    c.close();
                }
            } else {
                Log.d("cursor is null", "cursor");
            }
        }
    }

    private void createNotification(int id, String text) {
        NotificationCompat.Builder builder;
        Intent intent;
        long[] vibrationPattern = {100, 200};
        PendingIntent pendingIntent;
        if (notificationManager == null) {
            notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel channel = notificationManager.getNotificationChannel(Constants.ChannelId);
            if (channel == null) {
                channel = new NotificationChannel(Constants.ChannelId, Constants.ChannelName, importance);
                channel.enableVibration(true);
                channel.setVibrationPattern(vibrationPattern);
                notificationManager.createNotificationChannel(channel);
            }
            builder = new NotificationCompat.Builder(this, Constants.ChannelId);
            //intent = new Intent(this, MainActivity.class);
            intent = new Intent(this, DetailActivity.class);
            intent.putExtra(Constants.EXTRA_ID_JADWAL, String.valueOf(code));
            pendingIntent = PendingIntent.getActivity(this, id, intent, PendingIntent.FLAG_ONE_SHOT);
            builder.setContentTitle(getString(R.string.app_name))
                    .setSmallIcon(R.drawable.icon)
                    .setContentText(text)
                    .setDefaults(Notification.DEFAULT_ALL)
                    .setAutoCancel(true)
                    .setContentIntent(pendingIntent)
                    .setTicker(text)
                    .setVibrate(vibrationPattern)
                    .setPriority(Notification.PRIORITY_HIGH);

        } else {
            builder = new NotificationCompat.Builder(this);
           // intent = new Intent(this, MainActivity.class);
            intent = new Intent(this, DetailActivity.class);
            intent.putExtra(Constants.EXTRA_ID_JADWAL, String.valueOf(code));
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            pendingIntent = PendingIntent.getActivity(this, id, intent, PendingIntent.FLAG_ONE_SHOT);
            builder.setContentTitle(getString(R.string.app_name))
                    .setSmallIcon(R.drawable.icon)
                    .setContentText(text)
                    .setDefaults(Notification.DEFAULT_ALL)
                    .setAutoCancel(true)
                    .setContentIntent(pendingIntent)
                    .setTicker(text)
                    .setVibrate(vibrationPattern)
                    .setPriority(Notification.PRIORITY_HIGH);

        }

        Notification notification = builder.build();
        notificationManager.notify(id, notification);

    }
}
