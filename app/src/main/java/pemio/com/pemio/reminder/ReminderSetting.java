package pemio.com.pemio.reminder;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.widget.Toast;

import java.util.Calendar;

import pemio.com.pemio.converter.Converter;

import static android.content.Context.ALARM_SERVICE;
import static pemio.com.pemio.Constants.EXTRA_CODE_REQUEST;

public class ReminderSetting {

    public static void setReminder(Context context,Class<?> cl,int hour,int minutes,int notificationId){

        ComponentName receiver =new ComponentName(context,cl);
        PackageManager pm=context.getPackageManager();

        pm.setComponentEnabledSetting(receiver,PackageManager.COMPONENT_ENABLED_STATE_ENABLED, PackageManager.DONT_KILL_APP);
        final AlarmManager alarmManager = (AlarmManager) context.getSystemService(ALARM_SERVICE);

        Intent notifyInten = new Intent(context, cl);
        notifyInten.putExtra(EXTRA_CODE_REQUEST,notificationId);
        final PendingIntent pendingIntent = PendingIntent.getBroadcast(context, notificationId, notifyInten, 0);
        long repeatInterval = AlarmManager.INTERVAL_DAY;
        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(System.currentTimeMillis());
        c.set(Calendar.HOUR_OF_DAY, hour);
        c.set(Calendar.MINUTE, minutes);
        c.set(Calendar.SECOND,00);


        alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, c.getTimeInMillis(), repeatInterval, pendingIntent);
//        Toast.makeText(context, "Notifikasi Diaktifkan pukul : " + Converter.convertDigit(hour) + ":" + Converter.convertDigit(minutes), Toast.LENGTH_SHORT).show();

    }
    public static void cancelReminder(Context context,Class<?> cl,int notificationId){
        ComponentName reciever=new ComponentName(context,cl);
        PackageManager pm=context.getPackageManager();

        pm.setComponentEnabledSetting(reciever,PackageManager.COMPONENT_ENABLED_STATE_DISABLED,PackageManager.DONT_KILL_APP);
        Intent i=new Intent(context,cl);
        PendingIntent pendingIntent=PendingIntent.getBroadcast(context,notificationId,i,PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager alarmManager=(AlarmManager) context.getSystemService(ALARM_SERVICE);
        alarmManager.cancel(pendingIntent);
        pendingIntent.cancel();

    }

    }
