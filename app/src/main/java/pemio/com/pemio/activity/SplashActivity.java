package pemio.com.pemio.activity;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import pemio.com.pemio.PemioPreferences;
import pemio.com.pemio.R;

public class SplashActivity extends AppCompatActivity {
    boolean isLooggedIn = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                final boolean isLooggedIn = new PemioPreferences(SplashActivity.this).isLoggedIn();
                if (isLooggedIn) {
                    startActivity(new Intent(SplashActivity.this, MainActivity.class));
                } else {
                    startActivity(new Intent(SplashActivity.this, SignUpActivity.class));

                }
                finish();
            }
        }, 3000);


    }

    @Override
    protected void onResume() {
        super.onResume();

    }
}
