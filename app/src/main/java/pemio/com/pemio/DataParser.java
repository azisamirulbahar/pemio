package pemio.com.pemio;

import android.content.ContentValues;
import android.database.Cursor;

import pemio.com.pemio.data.Obat;

public class DataParser {
    public static ContentValues createContentValues(Obat obat) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(Obat.COLUMN_OBAT, obat.obat);
        contentValues.put(Obat.COLUMN_PENYAKIT, obat.penyakit);
        contentValues.put(Obat.COLUMN_SIGNA, obat.signa);

        contentValues.put(Obat.COLUMN_JAM, obat.jam);
        contentValues.put(Obat.COLUMN_MENIT, obat.menit);
        contentValues.put(Obat.COLUMN_INSERT_TIME, obat.insertTime);
        return contentValues;
    }

    public static void parse(Cursor c, Obat o) {
        o.id=c.getInt(c.getColumnIndex(Obat.COLUMN_ID));
        o.obat = c.getString(c.getColumnIndex(Obat.COLUMN_OBAT));
        o.penyakit = c.getString(c.getColumnIndex(Obat.COLUMN_PENYAKIT));
        o.jam=c.getInt(c.getColumnIndex(Obat.COLUMN_JAM));
        o.menit=c.getInt(c.getColumnIndex(Obat.COLUMN_MENIT));

    }
}
