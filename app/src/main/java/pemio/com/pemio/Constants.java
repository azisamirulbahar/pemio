package pemio.com.pemio;

import android.app.NotificationManager;

import static java.lang.Boolean.TRUE;

public class Constants {
    public static final boolean isDebug=TRUE;
   public static int notificationImportance= NotificationManager.IMPORTANCE_HIGH;
    public static final String ChannelId="pemio-notif-id";
    public static final String ChannelName="pemio-notif-name";

    public static final String SESSION_PREF_NAME="pemio-pref";
    public static final String SESSION_USERNAME="username";
    public static final String SESSION_RM="rm";
    public static final String SESSION_GENDER="gender";
    public static final String SESSION_USIA="usia";
    public static final String SESSION_EMPTY="empty";
    public static final String EXTRA_CODE_REQUEST="alarm_id";
    public static final String EXTRA_IS_AFTER_BOOT="is_after_boot";
public static final String EXTRA_ID_JADWAL="id_jadwal";

}
