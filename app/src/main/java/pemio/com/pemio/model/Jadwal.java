package pemio.com.pemio.model;

public class Jadwal {
String id;
String penyakit;
String namaObat;
String jamMinum;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPenyakit() {
        return penyakit;
    }

    public void setPenyakit(String penyakit) {
        this.penyakit = penyakit;
    }

    public String getNamaObat() {
        return namaObat;
    }

    public void setNamaObat(String namaObat) {
        this.namaObat = namaObat;
    }

    public String getJamMinum() {
        return jamMinum;
    }

    public void setJamMinum(String jamMinum) {
        this.jamMinum = jamMinum;
    }
}
