package pemio.com.pemio.data;


import android.arch.persistence.db.SupportSQLiteOpenHelper;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.DatabaseConfiguration;
import android.arch.persistence.room.InvalidationTracker;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.ContentProvider;
import android.content.Context;
import android.support.annotation.NonNull;


@Database(entities = {Obat.class},version = 1)
public abstract class PemioDatabase extends RoomDatabase{
//
  @SuppressWarnings("WeakerAccess")
    public abstract ObatDao obat();

    private static PemioDatabase sInstance;


    public static synchronized PemioDatabase getsInstance(Context context){
        if(sInstance==null){
            sInstance= Room.databaseBuilder(context.getApplicationContext(),PemioDatabase.class,"pemio").build();
          //  sInstance.populateInitialData();
        }
        return sInstance;
    }

//    private void populateInitialData() {
//        if(obat().count()==0){
//            Obat obat=new Obat();
//            beginTransaction();
//            try{
//                for(int i=0;i<Obat.O)
//            }
//        }
//    }

    @NonNull
    @Override
    protected SupportSQLiteOpenHelper createOpenHelper(DatabaseConfiguration config) {
        return null;
    }

    @NonNull
    @Override
    protected InvalidationTracker createInvalidationTracker() {
        return null;
    }

    @Override
    public void clearAllTables() {

    }




}
