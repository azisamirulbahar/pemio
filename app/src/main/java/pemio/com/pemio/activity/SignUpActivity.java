package pemio.com.pemio.activity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import pemio.com.pemio.PemioPreferences;
import pemio.com.pemio.R;
import pemio.com.pemio.databinding.ActivitySignUpBinding;
import pemio.com.pemio.fragment.TimePickerFragment;
import pemio.com.pemio.model.User;

public class SignUpActivity extends AppCompatActivity {
    ActivitySignUpBinding binding;
    PemioPreferences pemioPreferences;
    String noRm,gender;
    int usia;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding= DataBindingUtil.setContentView(this,R.layout.activity_sign_up);
        pemioPreferences=new PemioPreferences(this);
        binding.submit.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View view) {
                noRm=binding.inputRm.getText().toString();
                usia=Integer.parseInt(binding.inputUsia.getText().toString());
                gender=binding.inputGender.getSelectedItem().toString();
                User user=new User();
                user.setNoRm(noRm);
                user.setUsia(usia);
                user.setGender(gender);
                pemioPreferences.saveData(user);
                startActivity(new Intent(SignUpActivity.this,MainActivity.class));
      finish();
            }
        });
    }
}
