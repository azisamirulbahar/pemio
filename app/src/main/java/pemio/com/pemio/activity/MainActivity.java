package pemio.com.pemio.activity;

import android.app.DialogFragment;
import android.app.NotificationManager;
import android.content.Intent;
import android.database.Cursor;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.AsyncTaskLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.LinearLayoutManager;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import pemio.com.pemio.Constants;
import pemio.com.pemio.DataParser;
import pemio.com.pemio.R;
import pemio.com.pemio.adapter.JadwalAdapter;

import pemio.com.pemio.converter.Converter;
import pemio.com.pemio.data.Obat;
import pemio.com.pemio.databinding.ActivityMainBinding;


import pemio.com.pemio.fragment.TimePickerFragment;
import pemio.com.pemio.model.Jadwal;

import pemio.com.pemio.provider.PemioProvider;
import pemio.com.pemio.receiver.PemioReceiver;
import pemio.com.pemio.reminder.ReminderSetting;

public class MainActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor>, JadwalAdapter.onClickItem {
    ActivityMainBinding binding;
    private NotificationManager notificationManager;
    private int hour, minutes;
    private static final int NOTIFICATION_ID = 0;
    //private static final int NOTIFICATION_ID=(int) System.currentTimeMillis();
    JadwalAdapter adapter;
    EditText inputTime;
    String obat, time, penyakit;
    List<Jadwal> jadwalList;
    LayoutInflater inflater;
    View layoutDialog;
    int idInserted;
    private boolean doubleTap = false;
    boolean layoutObatFilled, layoutJamFilled;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        jadwalList = new ArrayList<>();

//        binding.submit.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                setAlarm();
//
//            }
//        });

        binding.fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialog();
            }
        });


        setRecyclerView();
    }

    public void showTimePicker(View v) {
        DialogFragment dialogFragment = new TimePickerFragment();
        dialogFragment.show(getFragmentManager(), "timePicker");


    }

    public void setTime(int i, int i1) {
//        binding.inputTime.setText(i + ":" + i1);
        hour = i;
        minutes = i1;

        time = Converter.convertDigit(hour) + ":" + Converter.convertDigit(minutes);


    }

    public void displayTime() {
        inputTime.setText(time);
    }


    @Override
    public void onBackPressed() {

        if (doubleTap) {
            super.onBackPressed();
            return;
        }
        this.doubleTap = true;
        Toast.makeText(this, getString(R.string.double_tap), Toast.LENGTH_SHORT).show();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                doubleTap = false;
            }
        }, 2000);
    }

    private void setRecyclerView() {
//        addDummy();
        jadwalList.clear();
        adapter = new JadwalAdapter(this, jadwalList, this);
        adapter.notifyDataSetChanged();
        binding.rvJadwal.setLayoutManager(new LinearLayoutManager(this));
        binding.rvJadwal.setHasFixedSize(true);

        binding.rvJadwal.setAdapter(adapter);

        getSupportLoaderManager().initLoader(1, null, this);
    }


    private void addData(Jadwal jadwal) {

        jadwal.setJamMinum(time);
        jadwal.setPenyakit(penyakit);
        jadwal.setNamaObat(obat);
        adapter.addItem(jadwal);
    }

    private void showDialog() {
//        LayoutInflater inflater = getLayoutInflater();
//
//        View layoutDialog = inflater.inflate(R.layout.dialog_jadwal, null);
        inflater = getLayoutInflater();

        layoutDialog = inflater.inflate(R.layout.dialog_jadwal, null);
        inputTime = layoutDialog.findViewById(R.id.inputTime);
        final TextInputLayout layoutObat = layoutDialog.findViewById(R.id.textLayoutObat);
        final TextInputLayout layoutJam = layoutDialog.findViewById(R.id.textLayoutJam);

        final AppCompatSpinner spinner = layoutDialog.findViewById(R.id.spinnerPenyakit);
        final EditText inputObat = layoutDialog.findViewById(R.id.input_obat);
//         EditText inputTime = layoutDialog.findViewById(R.id.inputTime);
        AppCompatButton appCompatButton = layoutDialog.findViewById(R.id.submit);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(layoutDialog);

        final AlertDialog dialog = builder.create();
        dialog.show();

        inputObat.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (i2 == 0) {
                    layoutObat.setError(getString(R.string.wajib_diisi));
                } else {
                    layoutObat.setError(null);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        inputTime.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (i2 == 0) {
                    layoutJam.setError(getString(R.string.wajib_diisi));
                } else {
                    layoutJam.setError(null);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        appCompatButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                penyakit = spinner.getSelectedItem().toString();
                obat = inputObat.getText().toString();

                if (TextUtils.isEmpty(obat)) {
                    layoutObat.setError(getString(R.string.wajib_diisi));
                }
                if (TextUtils.isEmpty(inputTime.getText().toString())) {
                    layoutJam.setError(getString(R.string.wajib_diisi));
                } else {
                    Jadwal jadwal = new Jadwal();

                    jadwal.setNamaObat(obat);
                    jadwal.setPenyakit(penyakit);
//                jadwal.setJamMinum(time);
                    Obat dataObat = new Obat();
                    dataObat.insertTime = System.currentTimeMillis();
                    dataObat.obat = obat;
                    dataObat.penyakit = penyakit;
                    dataObat.jam = hour;
                    dataObat.menit = minutes;
                    new SaveData().execute(dataObat);
                    addData(jadwal);
//                setAlarm();


                    dialog.cancel();
                }
            }
        });
        inputTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showTimePicker(view);
            }
        });

    }

    @Override
    public void onClick(int pos) {
        Intent i = new Intent(this, DetailActivity.class);
        i.putExtra(Constants.EXTRA_ID_JADWAL, jadwalList.get(pos).getId());
        startActivity(i);
        //  Toast.makeText(this,jadwalList.get(pos).getNamaObat(), Toast.LENGTH_SHORT).show();
    }


    class SaveData extends AsyncTask<Obat, Void, Uri> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected Uri doInBackground(Obat... obats) {
            Uri uri = getContentResolver().insert(PemioProvider.URI_PEMIO, DataParser.createContentValues(obats[0]));

            return uri;
        }

        @Override
        protected void onPostExecute(Uri uri) {
            super.onPostExecute(uri);
            idInserted = Integer.parseInt(uri.getLastPathSegment());
            ReminderSetting.setReminder(MainActivity.this, PemioReceiver.class, hour, minutes, idInserted);
        }
    }


    @NonNull
    @Override
    public Loader<Cursor> onCreateLoader(int id, @Nullable Bundle args) {
        return new AsyncTaskLoader<Cursor>(this) {

            @Override
            protected void onStartLoading() {
                super.onStartLoading();
                forceLoad();
            }

            @Nullable
            @Override
            public Cursor loadInBackground() {

                Cursor c = getContentResolver().query(PemioProvider.URI_PEMIO, null, null, null, null);
                return c;
            }
        };
    }

    @Override
    public void onLoadFinished(@NonNull Loader<Cursor> loader, Cursor data) {
        //    List<Jadwal> jadwalList = new ArrayList<>();
        if (data != null) {

            if (data.moveToFirst()) {
                while (!data.isAfterLast()) {
                    Jadwal jadwal = new Jadwal();
                    String id = String.valueOf(data.getInt(data.getColumnIndex(Obat.COLUMN_ID)));
                    String rowObat = data.getString(data.getColumnIndex(Obat.COLUMN_OBAT));
                    String rowPenyakit = data.getString(data.getColumnIndex(Obat.COLUMN_PENYAKIT));
                    int rowJam = data.getInt(data.getColumnIndex(Obat.COLUMN_JAM));
                    String rowMenit = String.valueOf(data.getInt(data.getColumnIndex(Obat.COLUMN_MENIT)));
                    int rowSigna = data.getInt(data.getColumnIndex(Obat.COLUMN_SIGNA));

                    jadwal.setId(id);
                    jadwal.setNamaObat(rowObat);
                    jadwal.setPenyakit(rowPenyakit);

                    jadwal.setJamMinum(String.valueOf(Converter.convertDigit(rowJam)) + ":" + Converter.convertDigit(Integer.parseInt(rowMenit)));
                    jadwalList.add(jadwal);
                    data.moveToNext();
                }
                adapter.addItem(jadwalList);

            }
        }
    }

    @Override
    public void onLoaderReset(@NonNull Loader<Cursor> loader) {

    }


}
