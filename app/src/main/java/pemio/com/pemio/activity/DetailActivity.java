package pemio.com.pemio.activity;

import android.content.Intent;
import android.database.Cursor;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.renderscript.ScriptGroup;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.AsyncTaskLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;

import java.security.Provider;

import pemio.com.pemio.Constants;
import pemio.com.pemio.DataParser;
import pemio.com.pemio.R;
import pemio.com.pemio.data.Obat;
import pemio.com.pemio.databinding.ActivityDetailBinding;
import pemio.com.pemio.model.Jadwal;
import pemio.com.pemio.provider.PemioProvider;

public class DetailActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Obat>{
ActivityDetailBinding binding;
String idjadwal;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding= DataBindingUtil.setContentView(this,R.layout.activity_detail);
        Intent i=getIntent();
        if(i.hasExtra(Constants.EXTRA_ID_JADWAL)){
            if(!TextUtils.isEmpty(i.getStringExtra(Constants.EXTRA_ID_JADWAL))){
idjadwal=i.getStringExtra(Constants.EXTRA_ID_JADWAL);
Log.d("loader-idjadwal",idjadwal);
getSupportLoaderManager().initLoader(1,null,this);
            }
        }
    }

    @NonNull
    @Override
    public Loader<Obat> onCreateLoader(int id, @Nullable Bundle args) {
        return new AsyncTaskLoader<Obat>(this) {
            @Override
            protected void onStartLoading() {
                super.onStartLoading();
                forceLoad();
            }

            @Nullable
            @Override
            public Obat loadInBackground() {
                Uri uri= Uri.parse(PemioProvider.URI_PEMIO+"/"+idjadwal);
                Log.d("loader",String.valueOf(uri));
                String[] projection = new String[]{Obat.COLUMN_ID, Obat.COLUMN_OBAT, Obat.COLUMN_PENYAKIT,Obat.COLUMN_JAM,Obat.COLUMN_MENIT};
                Obat obat=new Obat();
                Cursor c= getContentResolver().query(uri,projection,null,null,null);
             if(c!=null){
                 if(c.moveToFirst()){
                     DataParser.parse(c,obat);
                     Log.d("loader",obat.penyakit);
                 }
             }
             return obat;
            }
        };
    }

    @Override
    public void onLoadFinished(@NonNull Loader<Obat> loader, Obat data) {
Jadwal jadwal=new Jadwal();
jadwal.setPenyakit(data.penyakit);
jadwal.setNamaObat(data.obat);
jadwal.setJamMinum(data.jam+":"+data.menit);
binding.setJadwal(jadwal);
    }

    @Override
    public void onLoaderReset(@NonNull Loader<Obat> loader) {

    }
}
