package pemio.com.pemio.fragment;

import android.app.Dialog;
import android.app.DialogFragment;
import android.app.TimePickerDialog;

import android.content.DialogInterface;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.widget.TimePicker;


import java.util.Calendar;

import pemio.com.pemio.activity.MainActivity;

public class TimePickerFragment extends DialogFragment implements TimePickerDialog.OnTimeSetListener {




    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Calendar c = Calendar.getInstance();
        int hour = c.get(Calendar.HOUR_OF_DAY);
        int minute = c.get(Calendar.MINUTE);
        return new TimePickerDialog(getActivity(), this, hour, minute, DateFormat.is24HourFormat(getActivity()));

    }

    @Override
    public void onTimeSet(TimePicker timePicker, int i, int i1) {
        MainActivity mainActivity=(MainActivity) getActivity();
        mainActivity.setTime(i,i1);
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        MainActivity mainActivity=(MainActivity) getActivity();
        mainActivity.displayTime();
    }
}
