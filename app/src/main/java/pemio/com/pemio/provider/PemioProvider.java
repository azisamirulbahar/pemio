package pemio.com.pemio.provider;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.net.Uri;
import android.support.annotation.MainThread;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import pemio.com.pemio.data.ObatDao;
import pemio.com.pemio.data.PemioDatabase;
import pemio.com.pemio.data.Obat;

public class PemioProvider extends ContentProvider {
    public static final String AUTHORITY = "pemio.com.pemio.provider";

    public static final Uri URI_PEMIO = Uri.parse("content://" + AUTHORITY + "/" + Obat.TABLE_NAME);
    private static final int CODE_OBAT_ALL = 1;
    private static final int CODE_OBAT_ID = 2;

    private static final UriMatcher MATCHER = new UriMatcher(UriMatcher.NO_MATCH);

    static {
        MATCHER.addURI(AUTHORITY, Obat.TABLE_NAME, CODE_OBAT_ALL);
        MATCHER.addURI(AUTHORITY, Obat.TABLE_NAME + "/*", CODE_OBAT_ID);
    }

    @Override
    public boolean onCreate() {
        return true;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] strings, @Nullable String s, @Nullable String[] strings1, @Nullable String s1) {
        final int code = MATCHER.match(uri);
        if (code == CODE_OBAT_ID || code == CODE_OBAT_ALL) {
            final Context context = getContext();
            if (context == null) {
                return null;
            }

            ObatDao obatDao = PemioDatabase.getsInstance(context).obat();
            final Cursor cursor;
            Log.d("Uri",String.valueOf(uri));
            switch (code) {
                case CODE_OBAT_ALL:
                    cursor = obatDao.selectAll();
                    break;
                case CODE_OBAT_ID:
                    cursor = obatDao.selectById(ContentUris.parseId(uri));
                    Log.d("obatId",String.valueOf(ContentUris.parseId(uri)));
                    break;
                default:
                    cursor = null;
            }
            cursor.setNotificationUri(context.getContentResolver(), uri);
            return cursor;
        } else {
            throw new IllegalArgumentException("Unkown URI: " + uri);
        }
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        switch (MATCHER.match(uri)) {
            case CODE_OBAT_ALL:
                return "vnd.android.cursor.dir/" + AUTHORITY + "." + Obat.TABLE_NAME;
            case CODE_OBAT_ID:
                return "vnd.android.cursor.item/" + AUTHORITY + "." + Obat.TABLE_NAME;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues contentValues) {
        switch (MATCHER.match(uri)) {
            case CODE_OBAT_ALL:
                final Context context = getContext();
                if (context == null) {
                    return null;
                }
                final long id = PemioDatabase.getsInstance(context).obat().insert(Obat.fromContentValues(contentValues));
                context.getContentResolver().notifyChange(uri, null);
                return ContentUris.withAppendedId(uri, id);
            case CODE_OBAT_ID:
                throw new IllegalArgumentException("Invalid URI, cannot insert with ID:" + uri);

            default:
                throw new IllegalArgumentException("Unknown URI :" + uri);
        }
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String s, @Nullable String[] strings) {
        switch (MATCHER.match(uri)) {
            case CODE_OBAT_ALL:
                throw new IllegalArgumentException("Invalid URI,cannot update withoutID " + uri);
            case CODE_OBAT_ID:
                final Context context = getContext();
                if (context == null) {
                    return 0;
                }
                final int count = PemioDatabase.getsInstance(context).obat()
                        .deleteById(ContentUris.parseId(uri));
                return count;
            default:
                throw new IllegalArgumentException("unknown uri: " + uri);
        }
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues contentValues, @Nullable String s, @Nullable String[] strings) {
        switch (MATCHER.match(uri)) {
            case CODE_OBAT_ALL:
                throw new IllegalArgumentException("Invalid URI,cannot update without ID " + uri);
            case CODE_OBAT_ID:
                final Context context = getContext();
                if (context == null) {
                    return 0;
                }
                final Obat obat = Obat.fromContentValues(contentValues);
                obat.id = ContentUris.parseId(uri);
                final int count = PemioDatabase.getsInstance(context).obat().update(obat);
                context.getContentResolver().notifyChange(uri, null);
                return count;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }

    }
}
