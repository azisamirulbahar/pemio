package pemio.com.pemio.receiver;


import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.ContentObservable;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.LoaderManager;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.AsyncTaskLoader;
import android.support.v4.content.Loader;
import android.util.Log;
import android.widget.Toast;

import pemio.com.pemio.Constants;
import pemio.com.pemio.DataParser;
import pemio.com.pemio.R;
import pemio.com.pemio.activity.MainActivity;
import pemio.com.pemio.data.Obat;
import pemio.com.pemio.data.PemioDatabase;
import pemio.com.pemio.provider.PemioProvider;
import pemio.com.pemio.service.NotificationService;
import pemio.com.pemio.service.SchedulerService;

public class PemioReceiver extends BroadcastReceiver {
    int code;
    @Override
    public void onReceive(Context context, Intent intent) {
        Intent i = new Intent(context, NotificationService.class);
        if (intent.getAction() != null && context != null) {
            if (intent.getAction().equalsIgnoreCase(Intent.ACTION_BOOT_COMPLETED)) {
                Intent i2=new Intent(context,SchedulerService.class);
                i2.putExtra(Constants.EXTRA_IS_AFTER_BOOT,true);

               //  context.startService(i2);
                NotificationService.enqueueWork(context, i2);
                Log.d("afterboot-reciever",intent.getAction());
               // i.putExtra(Constants.EXTRA_IS_AFTER_BOOT, true);
             //   NotificationService.enqueueWork(context, i);
                return;
            }
        }
        code = intent.getIntExtra(Constants.EXTRA_CODE_REQUEST, 0);
        i = new Intent(context, SchedulerService.class);
        i.putExtra(Constants.EXTRA_CODE_REQUEST, code);
        Log.d("requestcode-receiver",String.valueOf(code));
       // context.startService(i);
        NotificationService.enqueueWork(context, i);
//        notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
//        this.context = context;
//        Intent contentIntnt = new Intent(context, MainActivity.class);
//
//        contentPendingIntent = PendingIntent.getActivity(context, NOTIFICATION_ID, contentIntnt, PendingIntent.FLAG_UPDATE_CURRENT);
//        code = intent.getIntExtra(Constants.EXTRA_CODE_REQUEST, 0);
//        Log.d("requestcode", String.valueOf(code));
//
//        String[] projection = new String[]{Obat.COLUMN_ID, Obat.COLUMN_OBAT, Obat.COLUMN_PENYAKIT};
//
//        Cursor c = context.getContentResolver().query(PemioProvider.URI_PEMIO, projection, Obat.COLUMN_ID + "=?", new String[]{String.valueOf(code)}, null);
//        Obat o = new Obat();
//        if (c != null) {
//            if (c.moveToFirst()) {
//                DataParser.parse(c, o);
//
//
//                NotificationCompat.Builder builder = new NotificationCompat.Builder(context, Constants.ChannelId)
//                        .setSmallIcon(R.drawable.icon)
//                        .setContentTitle(context.getString(R.string.app_name))
//                        .setContentText("Jangan lupa untuk minum obat : " + o.obat)
//                        .setContentIntent(contentPendingIntent)
//                        .setAutoCancel(true)
//                        .setPriority(Notification.PRIORITY_HIGH)
//                        .setDefaults(NotificationCompat.DEFAULT_ALL);
//
//                notificationManager.notify(NOTIFICATION_ID, builder.build());
//            }
//        } else {
//            Log.d("cursor is null", "cursor");
//        }

    }


}
