package pemio.com.pemio;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;

import pemio.com.pemio.model.User;

public class PemioPreferences {

    Context context;
    SharedPreferences sharedPreferences;
    public PemioPreferences(Context context){
        this.context=context;
        sharedPreferences=this.context.getSharedPreferences(Constants.SESSION_PREF_NAME,Context.MODE_PRIVATE);

    }

    public void saveData(User user){
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putString(Constants.SESSION_USERNAME,user.getUsername());
        editor.putString(Constants.SESSION_GENDER,user.getGender());
        editor.putString(Constants.SESSION_RM,user.getNoRm());
        editor.putInt(Constants.SESSION_USIA,user.getUsia());
        editor.commit();
    }
    public void clearAll(){
        sharedPreferences=context.getSharedPreferences(Constants.SESSION_PREF_NAME,Context.MODE_PRIVATE);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.clear();
        editor.commit();
    }
    public boolean isLoggedIn(){
        sharedPreferences=context.getSharedPreferences(Constants.SESSION_PREF_NAME,Context.MODE_PRIVATE);
      String noRm=sharedPreferences.getString(Constants.SESSION_RM,Constants.SESSION_EMPTY);
      if(noRm!=null||noRm!=""||noRm!=Constants.SESSION_EMPTY){
          return true;
      }else{
          return false;
      }

    }

}
